<?php

/**
 * @file
 * The installation instructions for the gmail Authentication Support.
 */

use PHPMailer\PHPMailer\PHPMailer;

/**
 * Implements hook_uninstall().
 */
function gmail_uninstall() {

  // Restore previous mail system.
  _disable_gmail();

  // Cleaning garbage.
  $config = \Drupal::service('config.factory');
  $gmail_config = $config->getEditable('gmail.settings');
  $gmail_config->delete();
}

/**
 * Add gmail timeout configuration and change default to 30.
 */
function gmail_update_8001() {
  \Drupal::configFactory()->getEditable('gmail.settings')
    ->set('gmail_timeout', 30)
    ->save(TRUE);
}

/**
 * Add gmail keepalive configuration and set default to FALSE.
 */
function gmail_update_8002() {
  \Drupal::configFactory()->getEditable('gmail.settings')
    ->set('gmail_keepalive', FALSE)
    ->save(TRUE);
}

/**
 * If mailsystem exists, disable gmail mailsystem automatically.
 */
function gmail_update_8004() {
  $mailsystem_enabled = \Drupal::moduleHandler()->moduleExists('mailsystem');
  if ($mailsystem_enabled) {
    _disable_gmail();
  }
}

/**
 * Implements hook_requirements().
 */
function gmail_requirements(string $phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    // Ensure PHPMailer exists.
    if (class_exists(PHPMailer::class)) {
      $mail = new PHPMailer();
    }
    if (empty($mail)) {
      $requirements['gmail_phpmailer'] = [
        'title' => (string) t('Gmail: PHPMailer Library'),
        'value' => (string) t('Missing'),
        'severity' => REQUIREMENT_ERROR,
        'description' => t('PHPMailer is Required for gmail to function.'),
      ];
      // If PHPMailer is not found, gmail should not be set as the mail system.
      _disable_gmail();

      return $requirements;
    }
    else {
      $required_version = '6.1.7';
      $installed_version = $mail::VERSION;
      $reflector = new \ReflectionClass('\PHPMailer\PHPMailer\PHPMailer');

      $requirements['gmail_phpmailer'] = [
        'title' => (string) t('gmail: PHPMailer library'),
        'value' => $installed_version,
        'description' => t('PHPMailer is located at %path', ['%path' => $reflector->getFileName()]),
      ];
      if (!version_compare($installed_version, $required_version, '>=')) {
        $requirements['gmail_phpmailer']['severity'] = REQUIREMENT_ERROR;
        $requirements['gmail_phpmailer']['description'] = (string) t("PHPMailer library @version or higher is required. Please install a newer version by executing 'composer update' in your site's root directory.", [
          '@version' => $required_version,
        ]);
        // If incorrect version, gmail should not be set as the mail system.
        _disable_gmail();
      }
      else {
      }
    }
  }
  return $requirements;
}

/**
 * Helper function to disable gmail and restore the default mailsystem.
 */
function _disable_gmail() {
  $config = \Drupal::service('config.factory');
  // Always make sure gmail is disabled.
  $gmail_config = $config->getEditable('gmail.settings');
  if (!$gmail_config->get('gmail_on')) {
    return;
  }
  // Set the internal gmail module config off.
  $gmail_config->set('gmail_on', FALSE)->save();

  // Set the default back to either the previous mail system or php_mail.
  $mail_config = $config->getEditable('system.mail');
  $current_default = $mail_config->get('interface.default');
  $system_default = 'php_mail';
  $previous_mail_system = $gmail_config->get('prev_mail_system') ?? NULL;
  if ($current_default == 'GmailSystem') {
    $default_interface = ($previous_mail_system && $previous_mail_system !== 'GmailSystem') ? $previous_mail_system : $system_default;
    $mail_config->set('interface.default', $default_interface)
      ->save();
  }
}
