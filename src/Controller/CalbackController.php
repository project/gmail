<?php

namespace Drupal\gmail\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Google\Client;
use Google\Service\Gmail;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides route responses for the Catalog module.
 */
class CalbackController extends ControllerBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $stack;

  /**
   * CatalogController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Configuration factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $stack
   *   The request service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, RequestStack $stack) {
    $this->configFactory = $configFactory;
    $this->stack = $stack;
  }

  /**
   * Create callback controller instance.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   *
   * @return \Drupal\robroy_rewards_system\Controller\RewardsController|static
   *   Return the rewards controller.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Get token from google callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request callback from google api server.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return RedirectResponse to gmail config url.
   */
  public function getToken(Request $request = NULL) {
    $gmailConfig = $this->configFactory->getEditable('gmail.settings');
    $config = [
      'client_id' => $gmailConfig->get('gmail_client_id'),
      'client_secret' => $gmailConfig->get('gmail_client_secret'),
    ];
    $client = new Client($config);
    $client->addScope(Gmail::GMAIL_SEND);
    $url = Url::fromRoute('gmail.callback');
    $url->setAbsolute(TRUE);
    $redirect_uri = $url->toString();
    $client->setRedirectUri($redirect_uri);
    $client->setPrompt('consent');
    $client->setApprovalPrompt('force');
    $client->setAccessType('offline');
    $code = $this->stack->getCurrentRequest()->query->get('code');
    if (isset($code)) {
      $token = $client->fetchAccessTokenWithAuthCode($code);
      foreach ($token as $key => $value) {
        $gmailConfig->set($key, $value)->save();
      }
    }
    $url = Url::fromRoute('gmail.config');
    $url->setAbsolute(TRUE);
    $redirect_uri = $url->toString();
    return new RedirectResponse($redirect_uri);
  }

}
