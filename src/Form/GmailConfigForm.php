<?php

namespace Drupal\gmail\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Google\Client;
use Google\Service\Gmail;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the gmail admin settings form.
 */
class GmailConfigForm extends ConfigFormBase {

  /**
   * Drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Email Validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs $messenger and $config_factory objects.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The D8 messenger object.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The Email Validator Service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, EmailValidatorInterface $email_validator, AccountProxyInterface $current_user, MailManagerInterface $mail_manager, ModuleHandlerInterface $module_handler) {
    $this->messenger = $messenger;
    $this->emailValidator = $email_validator;
    $this->currentUser = $current_user;
    $this->mailManager = $mail_manager;
    $this->moduleHandler = $module_handler;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('email.validator'),
      $container->get('current_user'),
      $container->get('plugin.manager.mail'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gmail_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('gmail.settings');

    // Don't overwrite the default if MailSystem module is enabled.
    $mailsystem_enabled = $this->moduleHandler->moduleExists('mailsystem');

    if ($config->get('gmail_on')) {
      $this->messenger->addMessage($this->t('gmail module is active.'));
      if ($mailsystem_enabled) {
        $this->messenger->addWarning($this->t('gmail module will use the mailsystem module upon config save.'));
      }
    }
    elseif ($mailsystem_enabled) {
      $this->messenger->addMessage($this->t('Gmail module is managed by <a href=":mailsystem">the mail system module</a>', [':mailsystem' => Url::fromRoute('mailsystem.settings')->toString()]));
    }
    else {
      $this->messenger->addMessage($this->t('gmail module is INACTIVE.'));
    }

    $this->messenger->addMessage($this->t('Disabled fields are overridden in site-specific configuration file.'), 'warning');

    if ($mailsystem_enabled) {
      $form['onoff']['gmail_on']['#type'] = 'value';
      $form['onoff']['gmail_on']['#value'] = 'mailsystem';
    }
    else {
      $form['onoff'] = [
        '#type'  => 'details',
        '#title' => $this->t('Install options'),
        '#open' => TRUE,
      ];
      $form['onoff']['gmail_on'] = [
        '#type' => 'radios',
        '#title' => $this->t('Set gmail as the default mailsystem'),
        '#default_value' => $config->get('gmail_on') ? 'on' : 'off',
        '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
        '#description' => $this->t('When on, all mail is passed through the gmail module.'),
        '#disabled' => $this->isOverridden('gmail_on'),
      ];

      // Force Disabling if PHPmailer doesn't exist.
      if (!class_exists(PHPMailer::class)) {
        $form['onoff']['gmail_on']['#disabled'] = TRUE;
        $form['onoff']['gmail_on']['#default_value'] = 'off';
        $form['onoff']['gmail_on']['#description'] = $this->t('<strong>gmail cannot be enabled because the PHPMailer library is missing.</strong>');
      }
    }

    $form['auth'] = [
      '#type' => 'details',
      '#title' => $this->t('Gmail API Authorization'),
      '#description' => $this->t('Get Oauth2 Client ID & Secret from https://console.cloud.google.com/apis/credentials?project='),
      '#open' => TRUE,
    ];
    $form['auth']['gmail_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('gmail_client_id'),
      '#description' => $this->t('Gmail Oauth Client ID.'),
      '#disabled' => $this->isOverridden('gmail_client_id'),
    ];
    $form['auth']['gmail_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('gmail_client_secret'),
      '#description' => $this->t("Gmail Oauth Client Secret."),
      '#disabled' => $this->isOverridden('gmail_client_secret'),
    ];

    $form['auth']['gmail_redirect_uri_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect Uri Host'),
      '#default_value' => $config->get('gmail_redirect_uri_host'),
      '#description' => $this->t("Redirect Uri Host"),
      '#disabled' => $this->isOverridden('gmail_redirect_uri_host'),
    ];

    $form['email_options'] = [
      '#type'  => 'details',
      '#title' => $this->t('E-mail options'),
      '#open' => TRUE,
    ];
    $form['email_options']['gmail_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail from address'),
      '#default_value' => $config->get('gmail_from'),
      '#description' => $this->t('The e-mail address that all e-mails will be from.'),
      '#disabled' => $this->isOverridden('gmail_from'),
    ];
    $form['email_options']['gmail_fromname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail from name'),
      '#default_value' => $config->get('gmail_fromname'),
      '#description' => $this->t('The name that all e-mails will be from. If left blank will use a default of: @name . Some providers (such as Office365) may ignore this field. For more information, please check gmail module documentation and your email provider documentation.',
          ['@name' => $this->configFactory->get('system.site')->get('name')]),
      '#disabled' => $this->isOverridden('gmail_fromname'),
    ];
    $form['email_options']['gmail_allowhtml'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow to send e-mails formatted as HTML'),
      '#default_value' => $config->get('gmail_allowhtml'),
      '#description' => $this->t('Checking this box will allow HTML formatted e-mails to be sent with the gmail protocol.'),
      '#disabled' => $this->isOverridden('gmail_allowhtml'),
    ];

    $form['email_test'] = [
      '#type' => 'details',
      '#title' => $this->t('Send test e-mail'),
      '#open' => TRUE,
    ];
    $form['email_test']['gmail_test_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail address to send a test e-mail to'),
      '#default_value' => '',
      '#description' => $this->t('Type in an address to have a test e-mail sent there.'),
    ];
    $form['email_test']['gmail_reroute_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail address to reroute all emails to'),
      '#default_value' => $config->get('gmail_reroute_address'),
      '#description' => $this->t('All emails sent by the site will be rerouted to this email address; use with caution.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Check if config variable is overridden by the settings.php.
   *
   * @param string $name
   *   Gmail settings key.
   *
   * @return bool
   *   Boolean.
   */
  protected function isOverridden($name) {
    $original = $this->configFactory->getEditable('gmail.settings')->get($name);
    $current = $this->configFactory->get('gmail.settings')->get($name);
    return $original != $current;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['gmail_from'] && !$this->emailValidator->isValid($values['gmail_from'])) {
      $form_state->setErrorByName('gmail_from', $this->t('The provided from e-mail address is not valid.'));
    }

    if ($values['gmail_test_address'] && !$this->emailValidator->isValid($values['gmail_test_address'])) {
      $form_state->setErrorByName('gmail_test_address', $this->t('The provided test e-mail address is not valid.'));
    }

    if ($values['gmail_reroute_address'] && !$this->emailValidator->isValid($values['gmail_reroute_address'])) {
      $form_state->setErrorByName('gmail_reroute_address', $this->t('The provided reroute e-mail address is not valid.'));
    }

    // If username is set empty, we must set both
    // username/password empty as well.
    if (empty($values['gmail_client_id'])) {
      $values['gmail_client_secret'] = '';
    }

    // A little hack. When form is presented,
    // the password is not shown (Drupal way of doing).
    // So, if user submits the form without changing the password,
    // we must prevent it from being reset.
    elseif (empty($values['gmail_client_secret'])) {
      $form_state->unsetValue('gmail_client_secret');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('gmail.settings');
    $mail_config = $this->configFactory->getEditable('system.mail');
    $mail_system = $mail_config->get('interface.default');

    // Updating config vars.
    if (isset($values['gmail_client_secret'])) {
      $config->set('gmail_client_secret', $values['gmail_client_secret']);
    }
    if (!$this->isOverridden('gmail_on')) {
      $config->set('gmail_on', $values['gmail_on'] == 'on')->save();
    }
    $config_keys = [
      'gmail_client_id',
      'gmail_redirect_uri_host',
      'gmail_client_secret',
      'gmail_from',
      'gmail_fromname',
      'gmail_allowhtml',
      'gmail_test_address',
      'gmail_reroute_address',
    ];
    foreach ($config_keys as $name) {
      if (!$this->isOverridden($name)) {
        $config->set($name, $values[$name])->save();
      }
    }

    // Set as default mail system if module is enabled.
    if ($config->get('gmail_on') ||
        ($this->isOverridden('gmail_on') && $values['gmail_on'] == 'on')) {
      if ($mail_system != 'GmailSystem') {
        $config->set('prev_mail_system', $mail_system);
      }
      $mail_system = 'GmailSystem';
      $mail_config->set('interface.default', $mail_system)->save();
    }
    else {
      $default_system_mail = 'php_mail';
      $mail_config = $this->configFactory->getEditable('system.mail');
      $default_interface = $mail_config->get('prev_mail_system') ? $mail_config->get('prev_mail_system') : $default_system_mail;
      $mail_config->set('interface.default', $default_interface)->save();
    }

    // If an address was given, send a test e-mail message.
    if ($test_address = $values['gmail_test_address']) {
      $params['subject'] = $this->t('Drupal gmail test e-mail');
      $params['body'] = [$this->t('If you receive this message it means your site is capable of using gmail to send e-mail.')];

      // If module is off, send the test message
      // with gmail by temporarily overriding.
      if (!$config->get('gmail_on')) {
        $original = $mail_config->get('interface');
        $mail_system = 'GmailSystem';
        $mail_config->set('interface.default', $mail_system)->save();
      }

      if ($this->mailManager->mail('gmail', 'gmail-test', $test_address, $this->currentUser->getPreferredLangcode(), $params)) {
        $this->messenger->addMessage($this->t('A test e-mail has been sent to @email via gmail. You may want to check the log for any error messages.', ['@email' => $test_address]));
      }
      if (!$config->get('gmail_on')) {
        $mail_config->set('interface', $original)->save();
      }

    }

    if (!$config->get('access_token')) {
      $config = [
        'client_id' => $config->get('gmail_client_id'),
        'client_secret' => $config->get('gmail_client_secret'),
      ];
      $client = new Client($config);
      $client->addScope(Gmail::GMAIL_SEND);
      $url = Url::fromRoute('gmail.callback');
      $url->setAbsolute(TRUE);
      $redirect_uri = $url->toString();
      $client->setRedirectUri($redirect_uri);
      $client->setPrompt('consent');
      $client->setApprovalPrompt('force');
      $client->setAccessType('offline');
      $authUrl = $client->createAuthUrl();
      $response = new RedirectResponse($authUrl);
      $response->send();
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gmail.settings',
    ];
  }

}
